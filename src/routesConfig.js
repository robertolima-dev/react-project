import Home from "./pages/home/Home";
import ApiCep from "./pages/dashboard/ApiCep";
import ApiCurrencies from "./pages/dashboard/ApiCurrencies";

const routesConfig = [
    {
        path: '/',
        component: Home,
        exact: true

    },
    {
        path: '/api-cep',
        component: ApiCep,
        exact: true

    },
    {
        path: '/api-moeda',
        component: ApiCurrencies,
        exact: true

    }
]

export default routesConfig;