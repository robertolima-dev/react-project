import React, { Component} from 'react'
import logoMenu from '../../assets/images/react.png'

class Home extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 mt-5">
                        <img className="img-fluid logo-home" src={logoMenu} alt="" />
                    </div>
                    <div className="col-md-6 align-self-center">
                        <h1>React.js</h1>
                        <p className="text-justify">Projeto de código aberto com React.js para prática. Este projeto utiliza axios como serviço HTTP, react-router-dom para criar rotas e bootstrap-react para Navbar. Faça uma pesquisa de endereço através do CEP utilizando comunicação com uma API aberta.</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home